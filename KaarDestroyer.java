package robosolutis;
import robocode.*;


/**
 * KaarDestroyer - a robot by Kennedy Anderson
 */
public class KaarDestroyer extends Robot
{

	public void run() {

		while(true) {
		
			ahead(200);
			turnGunRight(360);
			back(100);
			turnRight(90);
			turnGunRight(360);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		fire(1);
	}

	public void onHitByBullet(HitByBulletEvent e) {
		back(20);
		fire(1);
	}
	
	public void onHitWall(HitWallEvent e) {
		back(20);
		ahead(20);
		turnRight(180);
		ahead(60);
	}	
}
